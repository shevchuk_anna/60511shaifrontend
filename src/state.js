import {createStore} from "vuex";
import router from "./router";

const backendUrl = process.env.VUE_APP_BACKEND_URL;
const client_id = process.env.VUE_APP_CLIENT_ID;
const client_secret = process.env.VUE_APP_CLIENT_SECRET;

const store = createStore({
    state: {
        user: {},
        token: null,
        preLoading: false,
        loginError: false,
        loggedIn: false,
        dataPreLoading: true,
        room: {},
        pager: {
            currentPage: 1,
            perPage: 5,
        },
        validation: {},
        createRatingDialogVisible: false,
    },

    mutations: {
        setToken(state, token) {
            state.token = token;
        },

        getToken(context, token) {
            context.token = token;
        },

        setUser(context, user) {
            context.user = user;
        },

        setPreloading(context, is_load) {
            context.preLoading = is_load;
        },
        setDataPreloading(context, is_load) {
            context.dataPreLoading = is_load;
        },
        setLoginError(context, isError) {
            context.loginError = isError
        },
        setLoggedIn(context, isLoggedIn) {
            context.loggedIn = isLoggedIn
        },
        setRoom(context, room) {
            context.room = room
        },
        setPage(context, page) {
            context.pager.currentPage = page
        },
        setPager(context, pager) {
            context.pager = pager
        },
        setValidation(context, validation) {
            context.validation = validation;
        },
        setCreateRatingDialogVisible(context, visible) {
            context.createRatingDialogVisible = visible;
        },
        logout(context) {

            context.user = null;
            context.token = null;
            context.loggedIn = false;
            context.room = {};
            localStorage.removeItem('token')
            router.push('/')
        },
    },
    actions: {
        auth(context, {login, password}) {

            context.commit('setPreloading', true)
            window.axios.post(backendUrl + '/OAuthController/Authorize', {
                username: login,
                password: password,
                grant_type: 'password'
            }, {
                headers: {
                    Authorization: 'Basic ' + window.btoa(client_id + ':' + client_secret)
                }
            }).then((response) => {
                if (response.data.access_token) {
                    context.commit('setToken', response.data.access_token)
                    localStorage.setItem('token', response.data.access_token);
                    context.commit('setLoggedIn', true);
                    context.commit('setLoginError', false);
                    context.dispatch('getUser');
                } else {
                    context.commit('setLoginError', true)
                    context.commit('setPreloading', false)
                }
            })

        },
        getUser(context) {
            context.commit('setPreloading', true);
            console.log('get user')
            return window.axios.get(backendUrl + '/OAuthController/user', {
                headers: {
                    Authorization: 'Bearer ' + context.state.token
                }
            }).then((response) => {
                context.commit('setUser', response.data);
                context.commit('setPreloading', false);

            }).catch(error => {
                    if (error.response) context.commit('setLoggedIn', false);
                }
            )

        },
        getRoom(context) {
            console.log('get room')
            context.commit('setDataPreloading', true)
            const params = new URLSearchParams()
            params.append('per_page', context.state.pager.perPage)
            return window.axios.post(backendUrl + '/HomeApi/room?page_group1=' + context.state.pager.currentPage, params,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token
                    },
                }).then((response) => {
                context.commit('setRoom', response.data.Комната);
                context.commit('setPager', response.data.pager);
                context.commit('setDataPreloading', false)
                console.log(response.data.Комната)
                console.log(response.data.pager)

            })
        },
        createRoom(context, {ID_HOUSING, NumRoom, NumOfSeats, Cost}) {
            console.log('create room')
            context.commit('setDataPreloading', true)
            const params = new URLSearchParams()
            params.append('ID_HOUSING', ID_HOUSING)
            params.append('NumRoom', NumRoom)
            params.append('NumOfSeats', NumOfSeats)
            params.append('Cost', Cost)
            return window.axios.post(backendUrl + '/HomeApi/store', params,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token
                    },
                }).then((response) => {
                if (response.status === 201) {
                    console.log("Room created successfully");
                    context.commit("setValidation", {})
                    context.commit('setCreateRatingDialogVisible', false);
                    context.commit('setPage', context.state.pager.pageCount);
                    this.dispatch('getRoom');
                    router.push('/room');
                } else {
                    console.log(response.data)
                    context.commit("setValidation", response.data)
                }
            })
        },

        deleteRoom(context, {id}) {
            console.log('delete room')
            context.commit('setDataPreloading', true)
            return window.axios.get(backendUrl + '/HomeApi/delete/' + id,
                {
                    headers: {
                        Authorization: 'Bearer ' + context.state.token
                    },
                }).then((response) => {
                if (response.status === 200) {
                    console.log("Room deleted successfully");
                    context.commit('setPage', context.state.pager.pageCount);
                    this.dispatch('getRoom');
                    router.push('/room');
                } else {
                    console.log(response.data)
                }
            })
        }
    }
})

export default store;